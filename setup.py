from setuptools import setup, find_packages

setup(name='robust_sdp',
      version='1.0.0',
      install_requires=[
	      'cvxpy',
          ],
      packages=find_packages()
)
